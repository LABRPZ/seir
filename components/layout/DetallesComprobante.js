import React from 'react';
import styled from '@emotion/styled';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { es } from 'date-fns/locale';
import Link from 'next/link';


const ComprobanteContenedor = styled.li`
    padding: 1 rem;
    justify-content: space-between;
    aling-items: center;
    border: 1px solid #e1e1e1;
    background-color: #FFF;
    border-radius: 5px;
    margin: 5px;
    @media(min-width:768px){
        display: flex;
        justify-content: space-between;
    }
`;
const Informacion = styled.div`
    padding: 0;
    margin-right: 2rem;
    justify-content: space-between;
    aling-items: center;
    
`;
export const InputSubmit = styled.input`
    background-color: var(--naranja);
    border-radius: 5px;
    width: 100%;
    padding: 1.5rem;
    text-align: center;
    color: #FFF;
    font-size: 1.8rem;
    text-transform: uppercase;
    border: none;
    font-family: 'PT Sans', sans-serif;
    font-weight: 700;

    &:hover {
        cursor: pointer;
    }
`;

const ContenedorImagen = styled.div`
    aling-items:center;
`;
const Enlace = styled.a`
    font-size: 2rem;
    font-weight: bold;
    margin: 0;

    :hover {
        cursor: pointer;
    }
`;

const Imagen = styled.img`
    aling-items: center;
    padding: auto;
    margin: auto;
    width: 450px;
`;

const DetallesComprobante = ({comprobante}) => {
    const { id, nombre, carrera, numControl, urlimagen, estado, creado } = comprobante;
    return ( 
        <ComprobanteContenedor> 
            <Link href="/comprobantes/[id]" as={`/comprobantes/${id+','+carrera}`}>
            <Enlace>
                <Imagen src={urlimagen} />  
            </Enlace>
            </Link> 
            <Informacion>
                <div>
                    <h5>{formatDistanceToNow(new Date(creado), {locale: es})}</h5>
                    <h3>{nombre}</h3>
                    <h3>{numControl}</h3>
                    <h5>{carrera}</h5>
                </div>
            </Informacion>
        </ComprobanteContenedor>
     );
}
 
export default DetallesComprobante;