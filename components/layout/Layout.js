import React from 'react';
import Buscar from '../ui/Buscador';
import Header from '../layout/Header';
import { Global, css } from '@emotion/core';
import Head from 'next/head';



const Layout = props => {
    return ( 
        <>
            <Global 
                styles={css`
                    :root{
                        --gris: #3d3d3d;
                        --gris2: #6f6f6f;
                        --gris3: #e1e1e1;
                        --verdeDeg: #3F495F;
                        --azulDeg: #0F207B;
                        --naranja2: #FFB87E;
                        --naranja: #da552f;
                        --blanco: #DBECEA;
                    }
                    html {
                        font-size: 62.5%; /* permite usar los rem como medida a una escala de 1rem = 10px */
                        box-sizzing: border-box;
                    }
                    /* sirve pa eliminar los bordes que son agregados */
                    *, *:before, *:after {
                        box-sizzing: inherit;
                    }
                    body {
                        font-size: 1.6rem; /* 16px */
                        line-height: 1.5;
                        font-family: 'PT Sans', sans-serif;
                    }
                    h1, h2, h3 {
                        margin= 0 0 2rem 0;
                        line-height= 1.5;
                    }
                    h1 , h2 {
                        font-family: 'Roboto Slab', serif;
                        font-weight: 700;
                    }
                    h3 {
                        font-family: 'PT Sans', sans-serif;
                    }
                    ul {
                        list-style: none;
                        margin: 0;
                        padding: 0;
                    }
                    a {
                        text-decoration: none;
                    }
                    img {
                        max-width: 100%;
                    }
                `}
            />

            <Head>
                <html lang="es" />
                <title>Egresados-Next-js</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha256-l85OmPOjvil/SOvVt3HnSSjzF1TUMyT9eV0c2BzEGzU=" crossOrigin="anonymous" />
                <link href="https://fonts.googleapis.com/css2?family=PT+Sans:wght@400;700&family=Roboto+Slab:wght@400;700&display=swap" rel="stylesheet" />
                <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-core.min.js"></script>
                <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-pie.min.js"></script>
                <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-base.min.js"></script>
                {/* <link href="/static/css/app.css" rel="stylesheet" /> */}
            </Head>
            
            <Header />
            <main>
                {props.children}
            </main>
        </>
     );
}
 
export default Layout;