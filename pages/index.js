import React, { useEffect, useState, useContext }from 'react';
import Layout from '../components/layout/Layout';
import { FirebaseContext } from '../firebase';
import DetallesComprobante from '../components/layout/DetallesComprobante';
import styled from '@emotion/styled';


const ListadoComprobante = styled.div`
    background-color: #f3f3f3;
`;
const Contenedor = styled.div`
    max-width: 1200px;
    width: 90%;
    padding: 5rem 0;
    margin: 0 auto;
    
`;
const Ulcont = styled.ul`
`;


const Home = () => {

  const [comprobantes, guardarComprobantes ] = useState([]);

  const { firebase } = useContext(FirebaseContext);

  useEffect(() => {
    const obtenerComprobantes = () => {
      firebase.db.collection('comprobantes').orderBy('creado', 'desc').onSnapshot(manejarSnapshot)
    }
    obtenerComprobantes();
  }, []);

  function manejarSnapshot(snapshot) {
    const comprobantes = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    });
    guardarComprobantes(comprobantes);
  }
  
  return (
    <div> 
      <Layout>
        <ListadoComprobante>
          <Contenedor>
            <Ulcont>
              {comprobantes.map(comprobante => (
                <DetallesComprobante 
                  key={comprobante.id}
                  comprobante={comprobante}
                />
              ))}
            </Ulcont>
          </Contenedor>
        </ListadoComprobante>
      </Layout>
  </div>
  );
}
 
export default Home;
 