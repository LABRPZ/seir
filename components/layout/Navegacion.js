import React, { useContext } from 'react';
import Link from 'next/link';
import { FirebaseContext } from '../../firebase';


import styled from '@emotion/styled';

const Nav = styled.nav`
    padding-left: 1rem;
    padding-top: 10rem;
    a {
        font-size: 1.8rem;
        margin-left: 2rem;
        color: var(--naranja);
        font-family: 'PT Sans', sans-serif;
        color: var(--naranja2);
        &:last-of-type {
            margin-right: 0;
            
        }   
    }
    
`;


const Navegacion = () => {

    const { usuario } = useContext(FirebaseContext);
    return ( 
        <Nav>
            {usuario && (
                <Link href="/">Inicio</Link>
            )} 
            {usuario && (
                <Link href="/crear-ceremonia">Nueva Ceremonia</Link>
            )}
            {usuario && (
                <Link href="/Ceremonia">Ver ceremonias</Link>
            )}
            {usuario && (
                <Link href="/estadisticas">Estadisticas</Link>
            )} 
            <Link href="/comprobante">Comprobante</Link>
        </Nav>
     );
}
 
export default Navegacion;