import styled from '@emotion/styled';


export const Formulario = styled.form`
    max-width: 600px;
    width: 95%;
    margin: 5rem auto 0 auto;
    border: 1px solid var(gris2);

    fieldset{
        margin: 2rem;
        border: 1px solid #e1e1e1;
        font-size: 2 rem;
        padding: 2rem:
    }
`;

export const Campo = styled.div`
    margin-bottom: 2rem;
    display: flex;
    align-items: center;
    

    label {
        flex: 0 0 150px;
        font-size: 1.8rem;
    }

    input {
        border-radius: 5px;
        border: 1px solid var(--gris2);
        flex: 1;
        padding: 1rem;
        
    }
`;

export const InputSubmit = styled.input`
    background-color: var(--naranja);
    border-radius: 5px;
    width: 100%;
    padding: 1.5rem;
    text-align: canter;
    color: #FFF;
    font-size: 1.8rem;
    text-transform: uppercase;
    border: none;
    font-family: 'PT Sans', sans-serif;
    font-weight: 700;

    &:hover {
        cursor: pointer;
    }
`;

export const Error = styled.p`
    color: red;
    padding: 0;
    font-family: 'PT Sans', sans-serif;
    font-weight: 700;
    font-size: 1.2rem;
    text-align: right;
    text-transform: uppercase;
    margin: 0;
`;
