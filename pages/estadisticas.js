import React, { useEffect, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '../components/layout/Layout';
import { FirebaseContext } from '../firebase';
import Link from 'next/link';
import { es } from 'date-fns/locale';
import { css } from '@emotion/core';
import styled from '@emotion/styled';


const ContenedorGrafica = styled.div`
  width: 600px;
  height: 600px;
  
  margin: 5rem;
  padding: 0;
    @media(max-width:767px){
      width: 300px;
      height: 300px;
      display: flex;
      justify-content: space-between;
    }
`;

const Estadisticas = () => {
  var grafi = [];
  var arr_carreras = [
    "Ingeniería en Sistemas Computacionales",
    "Ingeniería de Materiales",
    "Ingeniería Bioquímica",
    "Ingeniería Electrónica",
    "Ingeniería Mecatrónica",
    "Ingeniería Mecánica",
    "Contador Público",
    "Ingeniería Industrial",
    "Administración de Empresas", 
  ];
  var aux = 0;
  var auxNombre = '';

  const [comprobantes, guardarComprobantes ] = useState([]);

  const { firebase } = useContext(FirebaseContext);
  

  useEffect(() => {
    const obtenerComprobantes = () => {
      firebase.db.collection('comprobantes').orderBy('creado', 'desc').onSnapshot(manejarSnapshot)
    }
    obtenerComprobantes();
  }, []);  
  function manejarSnapshot(snapshot) {
    const comprobantes = snapshot.docs.map(doc => {
      return {
        id: doc.id,
        ...doc.data()
      }
    });
    guardarComprobantes(comprobantes);
    arr_carreras.forEach(element => {
      comprobantes.forEach(carr => {
        if(carr.carrera==element){
          aux++;
          if(aux>=1){
            auxNombre=carr.carrera;
          }
        }
      });
      if(auxNombre!='' && aux>0){
        grafi.push({x: auxNombre , value: aux});
      }
      auxNombre='';
      aux=0;
    });

    setTimeout(() => {
      anychart.onDocumentReady(function () {
  
        // create data
        // create a chart and set the grafi
        // console.log(grafi);
        var chart = anychart.pie(grafi);
    
        // set the chart title
        chart.title("Egresados por: Carrera");
    
        // set the container id
        chart.container("container");
    
        // initiate drawing the chart
        chart.draw();
      });
      
    }, 500);
     
  }
  return (
    <div> 
      <Layout>
        <ContenedorGrafica id="container"></ContenedorGrafica>
      </Layout>
  </div>
  );
}
 
export default Estadisticas;