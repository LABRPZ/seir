import React, { useEffect, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Error404 from '../../components/layout/404';
import Layout from '../../components/layout/Layout';
import { FirebaseContext } from '../../firebase';
//import jsPDF from 'jspdf';
import Link from 'next/link';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { es } from 'date-fns/locale';
import { css } from '@emotion/core';
import styled from '@emotion/styled';


const ContenedorComprobantes = styled.div`
    max-width: 95%;
    margin: 2rem;
    @media(min-width: 768px){
        display: grid;
        grid-template-columns: 2fr 1fr;
        column-gap: 2rem;
    }
`;
const InputSubmit = styled.input`
    background-color: var(--naranja);
    border-radius: 5px;
    width: 95%;
    margin-top: 1rem;
    padding: 1.5rem;
    text-align: canter;
    color: #FFF;
    font-size: 1.8rem;
    text-transform: uppercase;
    border: none;
    font-family: 'PT Sans', sans-serif;
    font-weight: 700;

    &:hover {
        cursor: pointer;
    }
`;




const Comprobante = () => {


    //state de la ceremonia 
    const [ ceremonia, guardarCeremonia ] =useState({});
    //state del componente 
    const [ comprobante, guardarComprobante ] = useState({});
    const [ error, guardarError ] = useState(false);


    //Routing para obtener el id actual
    const router = useRouter();
    var { query: { id }} = router;

    const str = id == null ? id="" : id.split(',');
    const ida=str[0];
    const car=str[1];
    //context de firebase
    const { firebase } = useContext(FirebaseContext);
    useEffect(() => {
        if(ida){
            const obtenerComprobante = async () => {
                //query de el comprobante
                const comprobanteQuery = await firebase.db.collection('comprobantes').doc(ida);
                const comprobante = await comprobanteQuery.get();
                //query de la ceremonia


                if(comprobante.exists ) {
                    guardarComprobante(comprobante.data());
                    
                    
                } else{
                    guardarError(true);
                }
            }
            obtenerComprobante();
        }

    }, [ida]);

    useEffect(() => {
        if(car){
            const obtenerCeremonia = async () => {
                //query de la ceremonia
                const ceremoniaQuery = await firebase.db.collection('ceremonias').where('carrera', '==',car);
                const ceremonia = await ceremoniaQuery.get() .then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                        guardarCeremonia(doc.data());
                    });
                })
                .catch(function(error) {
                    console.log("Error getting documents: ", error);
                });;
            }
            obtenerCeremonia();
        }

    }, [car]);

    if(Object.keys(comprobante).length === 0 ) return 'Cargando...';
    const { nombre, carrera,email, numControl, urlimagen, estado, creado } = comprobante;
    const {  direccion, fecha, generacion } = ceremonia;




    //Generar el PDF de la invitacion para ser enviada al alumno correspondiente
   async function generarPDF() {
        // var imgData = '../../public/static/img/Itmorelia.png';
       /* var doc = new jsPDF()
     
        doc.setFontSize(20)
        doc.text(35, 25, 'Cordial invitacion a'+ nombre)
*/

        /*doc.setFontSize(12)
       
        doc.text(10, 36, 'A medida que comienzas tu viaje, lo primero que deberías hacer ')
        doc.text(10, 40, 'es tirar ese mapa comprado y comenzar a dibujar el tuyo.-Michael Dell.')
        doc.text(10, 46, 'Se complace en invitarlo a usted '+nombre+' con Numero de Control: '+numControl+' ')
        doc.text(10, 50, 'y a su familia al acto académico para despedir a nuestros alumnos')
        doc.text(10, 54, 'de la carrera de '+carrera)
        doc.text(10, 60, 'La ceremonia se llevará a cabo el '+ fecha+' en '+direccion)
        doc.text(10, 64,''+generacion)
       */

        var xhr = new XMLHttpRequest()

        // get a callback when the server responds
        xhr.addEventListener('load', () => {
          // update the state of the component with the result here
          console.log(xhr.responseText)
        })
        // open the request with the verb and the url
        xhr.open('POST', 'https://us-central1-egresados-react.cloudfunctions.net/sendMail?dest='+email+','+nombre+','+numControl+','+carrera+','+generacion
        )
        // send the request
        xhr.send()
      //  +'?nombre='+nombre+'?numControl='+numControl+'?carrera='+carrera+'?generacion='+generacion
    
    // doc.addImage(imgData, 'PNG', 15, 40, 180, 160)
        //await doc.save("Invitación.pdf");
        

    }
    

    return ( 
        <Layout>
            <>
                { error && <Error404 /> }

                <ContenedorComprobantes >
                    <div>
                        <img src={urlimagen} />
                        <h5
                            css={css`
                            text-align: center;
                            margin: 0;
                        `}
                        >{formatDistanceToNow(new Date(creado), {locale: es})}</h5>
                        
                        <h3 
                            css={css`
                            text-align: center;
                            margin: 0;
                        `}
                        >{nombre}</h3>
                        <h3
                        css={css`
                            text-align: center;
                            margin: 0;
                        `}  
                        >{numControl}</h3>
                        <h5 
                            css={css`
                            text-align: center;
                            margin: 0; 
                        `}
                        >{carrera}</h5>      
                    </div>
                    <div>
                    <form>
                    <Link href="/">
                        <InputSubmit 
                            type="submit"
                            value="Envia Invitacion"
                            onClick={generarPDF}
                        />  
                        </Link>            
                    </form>
                    </div>
                </ContenedorComprobantes>
            </>    
        </Layout>
     );
}
 
export default Comprobante;