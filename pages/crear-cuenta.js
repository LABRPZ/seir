import React, { useState} from 'react';
import Layout from '../components/layout/Layout';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import Router from 'next/router';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';

import firebase from '../firebase';

//validaciones
import useValidacion from '../hooks/useValidacion';
import validarCrearCuenta from '../validacion/validarCrearCuenta';

const Titulo = styled.h1`
  text-align: center;
  margin-top: 5rem;
`;

const STATE_INICIAL = {
  nombre: '',
  email: '',
  password: '',
  numControl: '',
}

const CrearCuenta = () => {

  const [ error, guardarError ] = useState(false);

  

  const { valores, errores, handleSubmit, handleChange, handleBlur } = 
        useValidacion(STATE_INICIAL, validarCrearCuenta, crearCuenta );

  const { nombre, email, password, numControl } = valores;

  async function crearCuenta() {
    try {
      await firebase.registrar(nombre, email, password, numControl);
      Router.push('/');
    } catch (error) {
      console.log('Hubo un error al crear el usuario', error.message);
      guardarError(error.message);
    }

  }

  return (
    <div> 
      <Layout>
        <>
          <Titulo>Crear Cuenta</Titulo>
          <Formulario
            onSubmit={handleSubmit}
          >

            {errores.nombre && <Error>{errores.nombre}</Error>}
            <Campo>
              <label htmlFor="nombre">Nombre</label>
              <input 
                type="text"
                id="nombre"
                placeholder="Escribe tu Nombre"
                name="nombre" 
                value={nombre}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Campo>

            {errores.email && <Error>{errores.email}</Error>}
            <Campo>
              <label htmlFor="email">Email</label>
              <input 
                type="email"
                id="email"
                placeholder="Escribe tu Email"
                name="email"
                value={email}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>

            {errores.password && <Error>{errores.password}</Error>}
            <Campo>
              <label htmlFor="password">Pasaword</label>
              <input 
                type="password"
                id="password"
                placeholder="Escribe tu Password"
                name="password"
                value={password}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>

            {errores.numControl && <Error>{errores.numControl}</Error>}
            <Campo>
              <label htmlFor="numControl">No. Control</label>
              <input 
                type="text"
                id="numControl"
                placeholder="Escribe tu No. Control"
                name="numControl" 
                value={numControl}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Campo>

            {error && <Error>{error}</Error>}
            <InputSubmit  
              type="submit" 
              value="Registrarse" 
            />
          </Formulario>
        </>
      </Layout>
  </div>
  );
}
 
export default CrearCuenta;