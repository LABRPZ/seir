export default function validarCrearCuenta(valores) {
    let errores = {}

    //Validar nombre del usuario
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio";
    }
    if(!valores.email){
        errores.email = "El email es obligatorio";
    }else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(valores.email)){
        errores.email = "El email no es valido";
    }
    if(!valores.password){
        errores.password = "El password es obligatorio";
    }else if(valores.password.length < 6 ){
        errores.password = "El password debe ser de 6 caracteres";
    }
    if(!valores.numControl){
        errores.numControl = "El numero de control es obligatorio";
    }else if(valores.numControl.length != 8 ){
        errores.numControl = "El numero de control debe ser de 8 caracteres";
    }

    return errores;
}