import React from 'react';
import styled from '@emotion/styled';
import { css } from '@emotion/core';

const InputText = styled.input`
    border: 1px solid var(--gris3);
    border-radius: 5px;
    padding: 1rem;
    min-width: 300px;
    background-color: transparent;
    ::placeholder { color: var(--blanco);}
`;
const InputSubmit = styled.button`
    padding-top: 5rem;
    height: 3rem;
    width: 3rem;
    display: block;
    background-size: 4rem;
    background-image: url('/static/img/buscar.png');
    background-repeat: no-repeat;
    position: relative;
    right: 1rem;
    top: 1px;
    background-color: transparent;
    border: none;
    text-indent: -9999px;
    color: var(--blanco); 

    &:hover {
        cursor: pointer;
    }
`;
const Form = styled.form`
    padding-top: 5rem;
    position: relative;
`;

const Buscar = () => {
    return ( 
        <Form>
            <InputText type="text" placeholder="Buscar Documento"/>
            <InputSubmit type="submit">Buscar</InputSubmit>
        </Form>
     );
}
 
export default Buscar;      