import React, { useEffect, useState, useContext }from 'react';
import Layout from '../components/layout/Layout';
import DetallesCeremonia from '../components/layout/DetallesCeremonia';
import useCeremonias from '../hooks/useCeremonias';
import styled from '@emotion/styled';



const ListadoCeremonia = styled.div`
    background-color: #f3f3f3;
`;
const Contenedor = styled.div`
    max-width: 1200px;
    width: 90%;
    padding: 5rem 0;
    margin: 0 auto;
`;
const Ulcont = styled.ul`
    background-color: #FFF;
`;

const Ceremonia = () => {
  const {ceremonias } = useCeremonias('carrera');
      
    return (
      <div> 
        <Layout>
          <ListadoCeremonia>
            <Contenedor>
              <Ulcont>
                {ceremonias.map(ceremonia => (
                  <DetallesCeremonia
                    key={ceremonia.id}
                    ceremonia={ceremonia}
                  />
                ))}
              </Ulcont>
            </Contenedor>
          </ListadoCeremonia>
        </Layout>
    </div>
    );
}

export default Ceremonia
