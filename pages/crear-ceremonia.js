import React, { useState, useContext} from 'react';
import Layout from '../components/layout/Layout';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import Router, { useRouter } from 'next/router';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';
import { FirebaseContext } from '../firebase';


//validaciones
import useValidacion from '../hooks/useValidacion';
import validarCeremonia from '../validacion/validarCeremonia';

const Titulo = styled.h1`
  text-align: center;
  margin-top: 5rem;
`;
const STATE_INICIAL = {
  generacion: '',
  carrera: '',
  direccion: '',
  fecha: '',
}

const CrearCeremonia = () => {
  const router = useRouter();
  const [ error, guardarError ] = useState(false);

  const { usuario, firebase } = useContext(FirebaseContext);
      // si el usuario no esta autenticado llevar al login
     

  const { valores, errores, handleSubmit, handleChange, handleBlur } = 
        useValidacion(STATE_INICIAL, validarCeremonia, CrearCeremonia );

  const { generacion, carrera, direccion, fecha } = valores;

  function CrearCeremonia() {
    if(!usuario) {
      return router.push('/login');
    }

    const ceremonia = {
      carrera,
      direccion,
      fecha,
      generacion,
  }
  // insertarlo en la base de datos
  firebase.db.collection('ceremonias').add(ceremonia);

  return router.push('/');


  }

  return (
    <div> 
      <Layout>
        <>
          <Titulo>Nueva Ceremonia</Titulo>
          <Formulario
            onSubmit={handleSubmit}
          >

            {errores.generacion && <Error>{errores.generacion}</Error>}
            <Campo>
              <label htmlFor="generacion">Generacion</label>
              <input 
                type="text"
                id="generacion"
                placeholder="Escribe la generacion"
                name="generacion" 
                value={generacion}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Campo>

            {errores.carrera && <Error>{errores.carrera}</Error>}
            <Campo>
              <label htmlFor="carrera">Carrera</label>
              <select
                  name="carrera"
                  value={carrera}
                  onChange={handleChange}
                  onBlur={handleBlur}
                >
                  <option name="carrera" value="Ingeniería en Sistemas Computacionales" label="Ingeniería en Sistemas Computacionales"></option>
                  <option name="carrera" value="Ingeniería de Materiales" label="Ingeniería de Materiales"></option>
                  <option name="carrera" value="Ingeniería Bioquímica" label="Ingeniería Bioquímica"></option>
                  <option name="carrera" value="Ingeniería Electrónica" label="Ingeniería Electrónica"></option>
                  <option name="carrera" value="Ingeniería Mecatrónica" label="Ingeniería Mecatrónica"></option>
                  <option name="carrera" value="Ingeniería Mecánica" label="Ingeniería Mecánica"></option>
                  <option name="carrera" value="Contador Público" label="Contador Público"></option>
                  <option name="carrera" value="Ingeniería Industrial" label="Ingeniería Industrial"></option>
                  <option name="carrera" value="Administración de Empresas" label="Administración de Empresas"></option>
                </select>
            </Campo>

            {errores.direccion && <Error>{errores.direccion}</Error>}
            <Campo>
              <label htmlFor="direccion">Dirección</label>
              <input 
                type="text"
                id="direccion"
                placeholder="Escribe la direccion"
                name="direccion"
                value={direccion}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>

            {errores.fecha && <Error>{errores.fecha}</Error>}
            <Campo>
              <label htmlFor="fecha">Fecha</label>
              <input 
                type="date"
                id="fecha"
                placeholder="Qué dia sera?"
                name="fecha" 
                value={fecha}
                onChange={handleChange}
                onBlur={handleBlur}
                
              />
            </Campo>

            {error && <Error>{error}</Error>}
            <InputSubmit  
              type="submit" 
              value="Crear Ceremonia" 
            />
          </Formulario>
        </>
      </Layout>
  </div>
  );
}
 
export default CrearCeremonia;