import React, { useContext} from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import Buscar from '../ui/Buscador';
import Navegacion from '../layout/Navegacion';
import Boton from '../ui/Boton';
import { FirebaseContext } from '../../firebase';


const ContenedorHeader = styled.div`
    max-width: 1200px;
    max-height: auto;
    width: 95%;
    margin: 0 auto; 
    @media(min-width:768px){
        display: flex;
        justify-content: space-between;
    }
`;

const Logo = styled.p`
    color: var(--naranja);
    font-size: 4rem;
    lene-height: 0;
    font-weidht: 700;
    font-family: 'Roboto Slab', serif;
    margin-right: 1rem;
    @media(min-width:768px){
        display: flex;
        justify-content: space-between;
    }
`;
const HeaderCss = styled.header`
    border-bottom: 2px solid var(--gris3);
    padding: 1rem 0;
    background: linear-gradient(70deg, var(--verdeDeg), var(--azulDeg));
    @media(min-width:768px){
        display: flex;
        justify-content: space-between;
    }
`;
const DivContenedorItems = styled.div`
    display: flex;
    align-items: center; 
`;
const InfoUsuario = styled.p`
    margin-right: 2rem;
    color: var(--blanco)
`;
const DivLogo = styled.div`
    display: flex;
    align-items: center;
`;

const Header = () => {

    const { usuario, firebase } = useContext(FirebaseContext);

    return ( 
        <HeaderCss>
        
            <ContenedorHeader>
                <DivLogo>
                    <Link href="/">
                    <Logo>Egresados
                    </Logo>
                    </Link>
                     
               
                </DivLogo> 
                <Navegacion />
                { usuario ? (
                    <>
                        <DivContenedorItems>
                        <InfoUsuario>Hola:{usuario.displayName}</InfoUsuario>
                        <Boton 
                            bgColor="true"
                            onClick={() => firebase.cerrarSesion()}
                            >Cerrar Sesión</Boton>
                        </DivContenedorItems>
                    </>
                ) : (
                    <>
                    <DivContenedorItems>
                        <Link href="/login">
                            <Boton bgColor="true" >Login</Boton>
                        </Link>
                        <Link href="/crear-cuenta">
                            <Boton>Registrarse</Boton>
                        </Link>
                    </DivContenedorItems>
                    </>
                )}
                
            </ContenedorHeader>
            
        </HeaderCss>
     );
}
 
export default Header;