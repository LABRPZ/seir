import React, { useState, useEffect, useContext } from 'react';
import { FirebaseContext } from '../firebase';

const useCeremonias = orden => {
    const [ceremonias, guardarCeremonia ] = useState([]);
  
    const { firebase } = useContext(FirebaseContext);
  
    useEffect(() => {
      const obtenerCeremonias = () => {
        firebase.db.collection('ceremonias').orderBy(orden, 'desc').onSnapshot(manejarSnapshot)
      }
      obtenerCeremonias();
    }, []);
  
    function manejarSnapshot(snapshot) {
      const ceremonias = snapshot.docs.map(doc => {
        return {
          id: doc.id,
          ...doc.data()
        }
      });
      guardarCeremonia(ceremonias);
    }
    return {
        ceremonias
    }
  
    
}

export default useCeremonias
