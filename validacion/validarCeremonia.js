export default function validarCeremonia(valores) {
    let errores = {}

    //Validar imagen del usuario
    if(!valores.generacion){
        errores.generacion = "Se requiere poner la generacion de la ceremonia";
    }
    if(!valores.carrera){
        errores.carrera = "Se requiere una carrera";
    }
    if(!valores.direccion){
        errores.direccion = "Se requiere una direccion";
    }

    
    if(!valores.fecha){
        errores.imagen = "Se requiere una fecha";
     
    }

    return errores;
}