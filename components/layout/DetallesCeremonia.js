import React from 'react';
import styled from '@emotion/styled';


const ComprobanteContenedor = styled.li`
    padding: 1 rem;
    border-botton: 1px solid var(--gris3);
    justify-content: space-between;
    aling-items: center;
    border-botton: 1px solid #e1e1e1;
    @media(min-width:768px){
        display: flex;
        justify-content: space-between;
    }
`;
const Informacion = styled.div`
    padding: 0;
    margin: 0;
    justify-content: space-between;
    aling-items: center;
    background-color: var(--gris3);
    
`;
export const InputSubmit = styled.input`
    background-color: var(--naranja);
    border-radius: 5px;
    width: 100%;
    padding: 1.5rem;
    text-align: center;
    color: #FFF;
    font-size: 1.8rem;
    text-transform: uppercase;
    border: none;
    font-family: 'PT Sans', sans-serif;
    font-weight: 700;

    &:hover {
        cursor: pointer;
    }
`;



const DetallesCeremonia = ({ceremonia}) => {
    const { carrera, direccion, fecha, generacion } = ceremonia;
    return ( 
        <div>
        <ComprobanteContenedor> 
            <Informacion>
                    <h3>{carrera}</h3>
                    <h3>Lugar de la ceremonia: {direccion}</h3>
                    <h3>Fecha: {fecha}</h3>
                    <h3>{generacion}</h3>

            </Informacion>
        </ComprobanteContenedor>
        </div>
     );
}
 
export default DetallesCeremonia;