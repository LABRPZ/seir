import React, { useState} from 'react';
import Layout from '../components/layout/Layout';
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import Router from 'next/router';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';

import firebase from '../firebase';

//validaciones
import useValidacion from '../hooks/useValidacion';
import validarIniciarSesion from '../validacion/validarIniciarSesion';

const Titulo = styled.h1`
  text-align: center;
  margin-top: 5rem;
`;

const STATE_INICIAL = {
  email: '',
  password: '',
}

const Login = () => {

  const [ error, guardarError ] = useState(false);

  

  const { valores, errores, handleSubmit, handleChange, handleBlur } = 
        useValidacion(STATE_INICIAL, validarIniciarSesion, iniciarSesion );

  const {  email, password } = valores;

  async function iniciarSesion(){
    try {
      const usuario = await firebase.login(email, password);
      Router.push('/')
    } catch (error){
        console.log('Hubo un error al iniciar Sesión', error.message);
        guardarError(error.message);
    }
  }

  return (
    <div> 
      <Layout>
        <>
          <Titulo>Iniciar Sesión</Titulo>
          <Formulario
            onSubmit={handleSubmit}
          >
            {errores.email && <Error>{errores.email}</Error>}
            <Campo>
              <label htmlFor="email">Email</label>
              <input 
                type="email"
                id="email"
                placeholder="Escribe tu Email"
                name="email"
                value={email}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>

            {errores.password && <Error>{errores.password}</Error>}
            <Campo>
              <label htmlFor="password">Pasaword</label>
              <input 
                type="password"
                id="password"
                placeholder="Escribe tu Password"
                name="password"
                value={password}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>

            {error && <Error>{error}</Error>}
            <InputSubmit  
              type="submit" 
              value="Iniciar Sesión" 
            />
          </Formulario>
        </>
      </Layout>
  </div>
  );
}
 
export default Login;