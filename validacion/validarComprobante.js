export default function validarComprobante(valores) {
    let errores = {}

    //Validar imagen del usuario
    if(!valores.nombre){
        errores.nombre = "El nombre es obligatorio";
    }

    if(!valores.carrera){
        errores.carrera = "La carrera es obligatorio";
    }

    if(!valores.numControl){
        errores.numControl = "El numero de control es obligatorio";
    }else if(valores.numControl.length != 8 ){
        errores.numControl = "El numero de control debe ser de 8 caracteres";
    }

    if(!valores.email){
        errores.email = "El email es obligatorio";
    }else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(valores.email)){
        errores.email = "El email no es valido";
    }
    // if(!valores.imagen){
    //     errores.imagen = "El imagen es obligatorio";
    // }

    return errores;
}