import React, { useState, useContext} from 'react';
import Layout from '../components/layout/Layout';
import styled from '@emotion/styled';
import Router, { useRouter } from 'next/router';
import FileUploader from 'react-firebase-file-uploader';
import { Formulario, Campo, InputSubmit, Error } from '../components/ui/Formulario';

import { FirebaseContext } from '../firebase';

//validaciones
import useValidacion from '../hooks/useValidacion';
import validarComprobante from '../validacion/validarComprobante';

const Titulo = styled.h1`
  text-align: center;
  margin-top: 5rem;
`;

const STATE_INICIAL = {
  nombre: '',
  carrera: '',
  numControl: '',
  email: '',
  //imagen: '',
}
const NuevoComprobante = () => {

  // state de las imagenes
  const [nombreimagen, guardarNombre] = useState('');
  const [subiendo, guardarSubiendo] = useState(false);
  const [ progreso, guardarProgreso ] = useState(0);
  const [urlimagen, guardarUrlImagen] = useState('');

  const [ error, guardarError] = useState(false);

  const { valores, errores, handleSubmit, handleChange, handleBlur } = useValidacion(STATE_INICIAL, validarComprobante, crearComprobante);

  const { nombre, email ,carrera, numControl, imagen } = valores;

  // hook de routing para redireccionar
  const router = useRouter();

  // context con las operaciones crud de firebase
  const { usuario, firebase } = useContext(FirebaseContext);

  function crearComprobante() {

    // si el usuario no esta autenticado llevar al login
    if(!usuario) {
      return router.push('/login');
    }

    // crear el objeto de nuevo comprobante
    const comprobante = {
        nombre,
        carrera,
        email,
        numControl,
        urlimagen,
        estado: false,
        creado: Date.now(),
        // creador: {
        //   id: usuario.uid,
        //   nombre: usuario.displayName
        // }
    }

    // insertarlo en la base de datos
    firebase.db.collection('comprobantes').add(comprobante);

    return router.push('/');

  }


  const handleUploadStart = () => {
      guardarProgreso(0);
      guardarSubiendo(true);
  }

  const handleProgress = progreso => guardarProgreso({ progreso });

  const handleUploadError = error => {
      guardarSubiendo(error);
      console.error(error);
  };

  const handleUploadSuccess = nombre => {
      guardarProgreso(100);
      guardarSubiendo(false);
      guardarNombre(nombre)
      firebase
          .storage
          .ref("comprobantes")
          .child(nombre)
          .getDownloadURL()
          .then(url => {
            console.log(url);
            guardarUrlImagen(url);
          } );
  };

  return (
    <div> 
      <Layout>
        <>
          <Titulo>Comprobante</Titulo>
          <Formulario
            onSubmit={handleSubmit}
            noValidate 
          >

            <fieldset>
              <legend>Datos Comprobante</legend>
            

              {errores.nombre && <Error>{errores.nombre}</Error>}
              <Campo>
                <label htmlFor="nombre">Nombre</label>
                <input 
                  type="text"
                  id="nombre"
                  placeholder="Escribe tu Nombre"
                  name="nombre" 
                  value={nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Campo>

              {errores.carrera && <Error>{errores.carrera}</Error>}
              <Campo>
                <label htmlFor="carrera">Carrera</label>
                <select
                  name="carrera"
                  value={carrera}
                  onChange={handleChange}
                  onBlur={handleBlur}
                >
                  <option name="carrera" value="Ingeniería en Sistemas Computacionales" label="Ingeniería en Sistemas Computacionales"></option>
                  <option name="carrera" value="Ingeniería de Materiales" label="Ingeniería de Materiales"></option>
                  <option name="carrera" value="Ingeniería Bioquímica" label="Ingeniería Bioquímica"></option>
                  <option name="carrera" value="Ingeniería Electrónica" label="Ingeniería Electrónica"></option>
                  <option name="carrera" value="Ingeniería Mecatrónica" label="Ingeniería Mecatrónica"></option>
                  <option name="carrera" value="Ingeniería Mecánica" label="Ingeniería Mecánica"></option>
                  <option name="carrera" value="Contador Público" label="Contador Público"></option>
                  <option name="carrera" value="Ingeniería Industrial" label="Ingeniería Industrial"></option>
                  <option name="carrera" value="Administración de Empresas" label="Administración de Empresas"></option>
                </select>
              </Campo>


              {errores.numControl && <Error>{errores.numControl}</Error>}
              <Campo>
                <label htmlFor="numControl">No. Control</label>
                <input 
                  type="text"
                  id="numControl"
                  placeholder="Escribe tu No. Control"
                  name="numControl" 
                  value={numControl}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Campo>

              <Campo>
                <label htmlFor="imagen">Imagen</label>
                <FileUploader
                  accept="image/*"
                  id="imagen"
                  name="imagen"
                  randomizeFilename
                  storageRef={firebase.storage.ref("comprobantes")}
                  onUploadStart={handleUploadStart}
                  onUploadError={handleUploadError}
                  onUploadSuccess={handleUploadSuccess}
                  onProgress={handleProgress}
                />
              </Campo>
              
              <Campo>
              <label htmlFor="email">Email</label>
              <input 
                type="text"
                id="email"
                placeholder="Escribe tu Email"
                name="email"
                value={email}
                onChange={handleChange} 
                onBlur={handleBlur}
              />
            </Campo>
              
            {errores.email && <Error>{errores.email}</Error>}
              {error && <Error>{error}</Error>}
              <InputSubmit  
                type="submit" 
                value="Registrarse" 
              />
            </fieldset>
          </Formulario>
        </>
      </Layout>
  </div>
  );
}
 
export default NuevoComprobante;